class Dog:
	"""
	Это класс собаки.
	"""

	def bark(self):
		"""
		Собака лает из метода!
		"""
		return True


animal = Dog()
print(animal.__doc__)
print(animal.bark.__doc__)